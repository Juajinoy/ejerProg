﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EjerProg
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page2 : ContentPage
	{
		public Page2 ()
		{
			InitializeComponent ();
		}

        //INICIO REGISTRAR
        async private void dataUser(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entryNombre.Text) || string.IsNullOrEmpty(entryApellido.Text) ||
                string.IsNullOrEmpty(entryCelular.Text))
            {
                //labelMessage.TextColor = Color.Red;
                //labelMessage.Text ="Debe escribir Usuario y contraseña.";

                await DisplayAlert("Error", "Debe escribir Nombre, Apellido y contraseña.", "OK");
            }
            else
            {
                labelNombre.TextColor =Color.Green;
                labelApellido.TextColor = Color.Green;
                labelCelular.TextColor = Color.Green;
                labelNombre.Text =entryNombre.Text;
                labelApellido.Text = entryApellido.Text;
                labelCelular.Text = entryCelular.Text;

                
            }
        }

        //FIN REGISTRAR
    }
}