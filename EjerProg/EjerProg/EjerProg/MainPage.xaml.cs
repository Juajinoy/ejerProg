﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EjerProg
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        //INICIO DE PARTIAL CLASS

        // Creacion del metodo que recibe dos parametros
        /*PARA MOSTRAR ALERTAR 
         DisplayAlert("Titulo de la alerta","Contenido de la alerta", "Boton que queremos colocar");
         DisplayAlert("Error", "Debe escribir Usuario y contraseña.", "OK");

            Navigation me permite navegar entre pantallas
             Navigation.PushAsync --- Para lanzar la pantalla
             
            Para pasar entre pantallas tenemos que sincronizarlas con async y await para transicion entre las 2 pantallas
            para esto se agrega async en el metodo y await en la propiedad de navigation
             
             */
             //INICION DE BOTON
        async private void ValidateUser(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entryUser.Text) || string.IsNullOrEmpty(entryPassword.Text))
            {
                //labelMessage.TextColor = Color.Red;
                //labelMessage.Text ="Debe escribir Usuario y contraseña.";

                await DisplayAlert("Error", "Debe escribir Usuario y contraseña.", "OK");
            }
            else
            {
                //labelMessage.TextColor = Color.Green;
                //labelMessage.Text = "Inicio de sesion exitosa.";
                await DisplayAlert("Bienvenido", "Inicio de sesion exitosa", "OK");

                await Navigation.PushAsync(new Page1());
            }
        }
        //FIN DE BOTON

        //INICIO DE TOMA DE DATOS



        //FIN DE TOMA DE DATOS


        //FINAL DE PARTIAL CLASS
    }
}
